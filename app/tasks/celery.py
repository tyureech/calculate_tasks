from celery import Celery

celery_app = Celery(
    "tasks",
    broker="redis://redis:6379",
    backend="redis://redis:6379",
    include=["app.tasks.tasks"],
)
