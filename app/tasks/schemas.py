from pydantic import BaseModel, validator


class SOperand(BaseModel):
    x: int
    y: int
    operator: str

    @validator("operator")
    def validate_special_characters(cls, value):
        if value not in ("+", "-", "*", "/"):
            raise ValueError("Поле может иметь только символы: +, -, *, /")
        return value


class SKey(BaseModel):
    id: str


class SResult(BaseModel):
    result: int | float


class SInfo(BaseModel):
    id: str
    status: str
