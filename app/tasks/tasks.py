from app.tasks.celery import celery_app


@celery_app.task
def calculate(x: int, y: int, operator: str) -> int:
    if operator == "+":
        return x + y
    elif operator == "-":
        return x - y
    elif operator == "*":
        return x * y
    elif operator == "/":
        if y == 0:
            raise ValueError("Division by zero is not allowed.")
        return x / y
    else:
        raise ValueError("Invalid operator. Supported operators: +, -, *, /")
