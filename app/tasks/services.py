import json
from typing import Dict, List

from celery import Task

from app.tasks.celery import celery_app


class TaskService:
    @staticmethod
    def add(func: Task, *args, **kwargs) -> str:
        return func.delay(*args, **kwargs)

    @staticmethod
    def get_by_id(id: str) -> str:
        redis = celery_app.backend.client
        task = redis.get(f"celery-task-meta-{id}")
        if task:
            return json.loads(task)

    @staticmethod
    def get_all() -> List[Dict]:
        redis = celery_app.backend.client
        keys = redis.keys("celery-task-meta-*")
        tasks = redis.mget(keys)
        tasks = list(map(lambda task: json.loads(task), tasks))
        return tasks

    @classmethod
    def get_info(cls) -> List[Dict]:
        tasks = cls.get_all()
        tasks_info = list()
        for task in tasks:
            tasks_info.append({"id": task["task_id"], "status": task["status"]})
        return tasks_info
