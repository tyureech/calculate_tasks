from typing import List

from fastapi import APIRouter, status

from app.exceptions import TaskNotFoundExeption, TaskIsStillRunning, ZeroDivisionExeption
from app.tasks.schemas import SInfo, SKey, SOperand, SResult
from app.tasks.services import TaskService
from app.tasks.tasks import calculate

router = APIRouter(prefix="/tasks")


@router.post("/", status_code=status.HTTP_201_CREATED)
async def create_task_calculate(operand: SOperand) -> SKey:
    if operand.operator == "/" and operand.y == 0:
        raise ZeroDivisionExeption
    task = TaskService.add(
        func=calculate, x=operand.x, y=operand.y, operator=operand.operator
    )
    return {"id": task.id}


@router.get("/{task_id}/", status_code=status.HTTP_200_OK)
async def get_task_result(task_id: str) -> SResult:
    task = TaskService.get_by_id(id=task_id)
    if not task:
        raise TaskNotFoundExeption
    if task["status"] != "SUCCESS":
        raise TaskIsStillRunning
    return {"result": task["result"]}


@router.get("/", status_code=status.HTTP_200_OK)
async def get_all_tasks() -> List[SInfo]:
    return TaskService.get_info()
