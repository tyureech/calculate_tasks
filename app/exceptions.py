from fastapi import HTTPException, status


class BaseException(HTTPException):
    status_code = 500
    detail = ""

    def __init__(self):
        super().__init__(status_code=self.status_code, detail=self.detail)


class ZeroDivisionExeption(BaseException):
    status_code = status.HTTP_422_UNPROCESSABLE_ENTITY
    detail = "Нельзя делить на ноль!"


class TaskNotFoundExeption(BaseException):
    status_code = status.HTTP_404_NOT_FOUND
    detail = "Задача не найдена!"


class TaskIsStillRunning(BaseException):
    status_code = status.HTTP_202_ACCEPTED
    detail = "Задача все еще выполняется"
