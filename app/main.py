import sentry_sdk
from fastapi import FastAPI

from app.tasks.routers import router as router_tasks

app = FastAPI()

app.include_router(router_tasks)

sentry_sdk.init(
    dsn="https://6a439e5867e549d49bba433dea1485b4@o4505760277463040.ingest.sentry.io/4506009759907840",
    traces_sample_rate=1.0,
    profiles_sample_rate=1.0,
)
