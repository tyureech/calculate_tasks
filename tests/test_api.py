import pytest
from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


@pytest.mark.parametrize(
    "x, y, operator, status_code_create_task, status_code_get_result, result",
    [
        (2, 5, "+", 201, 200, 7),
        (10, 5, "*", 201, 200, 50),
        (2, 3, "-", 201, 200, -1),
        (60, 15, "/", 201, 200, 4),
        (1, 5, "/", 201, 200, 0.2),
        (1, 0, "/", 422, None, None),
        (60, 15, "A", 422, None, None),
    ],
)
def test_create_task_calculate_end_get_task_result(
    x: int,
    y: int,
    operator: str,
    status_code_create_task: int,
    status_code_get_result: int | None,
    result: int | None
):
    task_response = client.post(
        "/tasks/",
        json={"x": x, "y": y, "operator": operator},
    )
    assert task_response.status_code == status_code_create_task
    task = task_response.json()
    if task.get("id"):
        result_responce = client.get(f"/tasks/{task['id']}")
        assert result_responce.status_code == status_code_get_result
        assert result_responce.json()["result"] == result


@pytest.mark.parametrize(
    "id, status_code",
    [
        ("123145678", 404),
    ]
)
def test_get_task_result(id, status_code):
    responce = client.get(f"/tasks/{id}")
    assert responce.status_code == status_code


def test_get_all_tasks():
    response = client.get("/tasks/")
    assert response.status_code == 200
